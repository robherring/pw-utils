#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2020 Rob Herring <robh@kernel.org>
# Based on code from b4
#

import os
import sys
import re
import email
import email.message
import json

from string import Template
from email import utils
from email import policy
from pathlib import Path

REPLY_TEMPLATE = """
On ${date}, ${fromname} wrote:
${quote}

${summary}
"""

def format_addrs(pairs):
    addrs = set()
    for pair in pairs:
        addrs.add(email.utils.formataddr(pair))
    return ', '.join(addrs)

def make_reply(msg_buf, summary="", reply_template=REPLY_TEMPLATE):
    if not os.environ.get('EMAIL'):
        print("Must set EMAIL env var", file=sys.stderr)
        exit(1)

    try:
        orig_msg = email.message_from_bytes(msg_buf, policy=policy.default)
    except:
        print("Unable to read message", file=sys.stderr)
        exit(1)

    reply = dict()
    reply['date'] = orig_msg['date']
    reply['summary'] = summary

    myname = utils.parseaddr(os.environ.get('EMAIL'))[0]
    myemail = utils.parseaddr(os.environ.get('EMAIL'))[1]

    from_header = email.header.decode_header(utils.parseaddr(orig_msg['from'])[0])[0]
    if isinstance(from_header[0], bytes):
        reply['fromname'] = from_header[0].decode(from_header[1])
    else:
        reply['fromname'] = from_header[0]
    reply['fromemail'] = utils.parseaddr(orig_msg['from'])[1]

    if len(reply['fromname']) == 0:
        del reply['fromname']
        reply['fromname'] = str(reply['fromemail'])

    quotelines = list()
    diff_re = re.compile('^(diff --git|Index: )')

    body = orig_msg.get_body(preferencelist='plain').get_content(errors='strict')

    for line in body.split('\n'):
        if diff_re.match(line):
            break;
        quotelines.append('> %s' % line.rstrip())

    reply['quote'] = '\n'.join(quotelines)


    body = Template(reply_template).safe_substitute(reply)
    # Conform to email standards
    body = body.replace('\n', '\r\n')
    msg = email.message_from_string(body)
    msg['From'] = format_addrs([(myname, myemail)])

    # Deal with To header exception on 'unlisted-recipients:; (no To-header on input)'
    try:
        orig_msg.get('to', None)
    except:
        del orig_msg['to']

    allcc = utils.getaddresses(orig_msg.get_all('to', []) + orig_msg.get_all('cc', []) )

#    print(allcc, file=sys.stderr)

    # Remove ourselves and original sender from allto or allcc
    for entry in list(allcc):
        if entry[1] == myemail or entry[1] == reply['fromemail']:
            allcc.remove(entry)

    msg['To'] = format_addrs([utils.parseaddr(orig_msg['from'])])
    msg['Cc'] = format_addrs(allcc)

    msg['In-Reply-To'] = '%s' % orig_msg['Message-Id']
    if orig_msg['references']:
        msg['References'] = '%s %s' % (orig_msg['references'], orig_msg['Message-Id'])
    else:
        msg['References'] = '%s' % orig_msg['Message-Id']

    if orig_msg['subject'].find('Re: ') < 0:
        msg['Subject'] = 'Re: %s' % orig_msg['subject']
    else:
        msg['Subject'] = orig_msg['subject']

    return msg

if __name__ == "__main__":

    reply = make_reply(sys.stdin.buffer.read())
    print(reply.as_string(unixfrom=True))
