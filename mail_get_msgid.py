#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-2.0-only
# Copyright (C) 2022 Rob Herring <robh@kernel.org>
#

import sys
import mailbox
import argparse
import tempfile

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("msgid", type=str, help="Message-Id of message to retrieve")
    args = ap.parse_args()

    fp = tempfile.NamedTemporaryFile()
    fp.write(sys.stdin.buffer.read())

    msgs = mailbox.mbox(fp.name)
    for msg in msgs:
        if args.msgid in msg['Message-Id']:
            print(msg.as_string())
            break
